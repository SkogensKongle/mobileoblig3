package com.mobile3;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity implements SensorEventListener {

    CustomDrawableView mCustomDrawableView = null;
    public float xPosition, xAcceleration,xVelocity = 0.0f;
    public float yPosition, yAcceleration,yVelocity = 0.0f;
    public float xmax,ymax;
    private Bitmap mBitmap;
    private SensorManager sensorManager = null;
    public float frameTime = 0.666f;
    private Vibrator vibrator;
    private ToneGenerator toneGenerator;
    final int dstWidth = 50;
    final int dstHeight = 50;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);

        //Sett tone and vibrate
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        //Set FullScreen & portrait
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Get a reference to a SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        assert sensorManager != null;
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                SensorManager.SENSOR_DELAY_GAME);

        mCustomDrawableView = new CustomDrawableView(this);
        setContentView(mCustomDrawableView);

        //Calculate Boundry
        Display display = getWindowManager().getDefaultDisplay();
        xmax = (float)display.getWidth() - 50;
        ymax = (float)display.getHeight() - 50;

        xPosition = (float)display.getWidth() / 2;
        yPosition = (float)display.getHeight() / 2;
    }

    // This method will update the UI on new sensor events
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ORIENTATION) {
                //Set sensor values as acceleration
                yAcceleration = sensorEvent.values[1];
                xAcceleration = sensorEvent.values[2];
                updateBall();
            }
        }
    }

    private void updateBall() {

        boolean collision = false;
        //Calculate new speed
        xVelocity += ((xAcceleration/600) * frameTime);
        yVelocity += ((yAcceleration/600) * frameTime);

        //Calc distance travelled in that time
        float xS = (xVelocity/*/2*/)*frameTime;
        float yS = (yVelocity/*/2*/)*frameTime;

        //Add to position negative due to sensor
        //readings being opposite to what we want!
        xPosition -= xS;
        yPosition -= yS;

        if (xPosition + dstWidth > xmax) {
            xPosition = xmax - dstWidth;
            xVelocity = -xVelocity;
            collision = true;
        } else if (xPosition < 50) {
            xPosition = 50;
            xVelocity = -xVelocity;
            collision = true;
        }
        if (yPosition + dstHeight > ymax) {
            yPosition = ymax - dstHeight;
            yVelocity = -yVelocity;
            collision = true;
        } else if (yPosition < 50) {
            yPosition = 50;
            yVelocity = -yVelocity;
            collision = true;
        }

        if(collision){
            onCollision(collision);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onStop()
    {
        // Unregister the listener
        sensorManager.unregisterListener(this);
        super.onStop();
    }

    public class CustomDrawableView extends View
    {
        private Paint rectPaint;
        public CustomDrawableView(Context context)
        {
            super(context);
            Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
            mBitmap = Bitmap.createScaledBitmap(ball, dstWidth, dstHeight, true);
            rectPaint = new Paint();
            rectPaint.setColor(Color.BLACK);

        }

        protected void onDraw(Canvas canvas)
        {
            //final Bitmap bitmap = mBitmap;
            canvas.drawRect(50, 50, xmax, ymax, rectPaint);
            canvas.drawBitmap(mBitmap, xPosition, yPosition, null);
            invalidate();
        }
    }

    public void onCollision(boolean maby)
    {
        if(maby){
            toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP);
            vibrator.vibrate(10);
        }
    }
}
